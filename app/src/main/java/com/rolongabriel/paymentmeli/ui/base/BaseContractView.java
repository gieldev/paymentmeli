package com.rolongabriel.paymentmeli.ui.base;

import android.content.DialogInterface;

public interface BaseContractView {
    void showToastView(String stringID, boolean longDuration);
    void showToastView(int stringID, boolean longDuration);
    void showLoadingView();
    void dismissLoadingView();
    void showDialogView(int arrayId, DialogInterface.OnClickListener itemSelected);

}