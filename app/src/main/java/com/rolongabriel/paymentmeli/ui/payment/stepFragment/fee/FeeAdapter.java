package com.rolongabriel.paymentmeli.ui.payment.stepFragment.fee;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rolongabriel.paymentmeli.R;
import com.rolongabriel.paymentmeli.common.OnItemSelected;
import com.rolongabriel.paymentmeli.data.model.FeeData;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.bank.BankAdapter;

import java.util.ArrayList;

public class FeeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private ArrayList<FeeData> dataList;
    private OnItemSelected<FeeData> onItemSelected;
    private final int TYPE_DATA=1;
    private final int TYPE_DATA_EMPTY=3;


    public FeeAdapter(ArrayList<FeeData> dataList, OnItemSelected<FeeData> onItemSelected){
        setData(dataList);
        this.onItemSelected = onItemSelected;
    }

    public void setData( ArrayList<FeeData> dataList){
        this.dataList = dataList;
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(final int position) {



        if(dataList == null ||dataList.isEmpty())
            return TYPE_DATA_EMPTY;

        return TYPE_DATA;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(
                viewGroup.getContext());

        View v;
        switch (i){
            case TYPE_DATA:
                 v = inflater.inflate(R.layout.row_fee, viewGroup, false);
                ViewHolderData vh = new ViewHolderData(viewGroup.getContext(), v);
                return vh;

            default:
            case TYPE_DATA_EMPTY:
                v = inflater.inflate(R.layout.row_empty, viewGroup, false);
                return new ViewHolderDataEmpty(viewGroup.getContext(), v);

        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if(viewHolder instanceof ViewHolderData) {
            ViewHolderData holderData = (ViewHolderData) viewHolder;
            FeeData feeData = dataList.get(i);
            holderData.layout.setTag(feeData);
            holderData.recommendedTV.setText(feeData.getRecommendedMessage());
        }else{
            ViewHolderDataEmpty holderDataEmpty = (ViewHolderDataEmpty) viewHolder;

            if(dataList== null){
                holderDataEmpty.messageTV.setText(holderDataEmpty.context.getString(R.string.loading));
                return;
            }

            if(dataList.isEmpty()){
                holderDataEmpty.messageTV.setText(holderDataEmpty.context.getString(R.string.data_empty));
                return;
            }
        }


    }


    @Override
    public int getItemCount() {
        if(dataList== null || dataList.isEmpty())
            return 1;
        else
        return dataList.size();
    }

    public class ViewHolderData extends RecyclerView.ViewHolder {
        Context context;
         TextView recommendedTV;
         View layout;

        public ViewHolderData(Context context, final View v) {
            super(v);
            this.context = context;
            layout = v;
            recommendedTV =  v.findViewById(R.id.recommendedTV);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FeeData feeData = (FeeData) view.getTag();
                    onItemSelected.onSelected(feeData);
                }
            });
        }
    }

    public class ViewHolderDataEmpty extends RecyclerView.ViewHolder {
        Context context;
        TextView messageTV;
        View layout;

        public ViewHolderDataEmpty(Context context, final View v) {
            super(v);
            this.context = context;
            layout = v;
            messageTV =  v.findViewById(R.id.messageTV);
        }
    }

}










