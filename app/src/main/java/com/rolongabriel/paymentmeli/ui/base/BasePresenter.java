package com.rolongabriel.paymentmeli.ui.base;

public class BasePresenter <View > {

    protected View view;

    public BasePresenter(View view){


        this.view = view;
    }
}