package com.rolongabriel.paymentmeli.ui.base;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.rolongabriel.paymentmeli.R;


public abstract class BaseActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener  {

    protected SwipeRefreshLayout swipeRefreshLayout;
    private ImageView blockScreen;
    private int countLoading = 0;

    protected BaseFragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutID());

        initLoading();
        init();
    }

    private void initLoading() {
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        if(swipeRefreshLayout!= null){
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark),getResources().getColor(R.color.yellow),getResources().getColor(R.color.blue_dark),getResources().getColor(R.color.blue));
        }

        blockScreen =  findViewById(R.id.blockScreen);

    }


    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    protected abstract int getLayoutID();

    protected abstract void init();

    protected void loadFragment(int containerID, BaseFragment fragment, boolean backStact) {

        FragmentManager manager = getSupportFragmentManager();

        if(backStact){
            manager.beginTransaction().
                    replace(containerID, fragment, fragment.getcurrentTag()).addToBackStack(fragment.getcurrentTag()).
                    commit();
        }else{
            manager.beginTransaction().
                    replace(containerID, fragment, fragment.getcurrentTag()).
                    commit();
        }

        currentFragment = fragment;

    }

    protected void showToast(int stringID, boolean longDuration) {
        showToast(getString(stringID), longDuration);
    }

    protected void showToast(String stringID, boolean longDuration) {
        try {
            int duration = longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
            Toast.makeText(this, stringID, duration).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void showLoading() {

        if (swipeRefreshLayout == null || blockScreen == null)
            return;

        try {
            blockScreen.setVisibility(View.VISIBLE);
            if (!swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(true);
            }
            countLoading++;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void dismissLoading() {

        if (swipeRefreshLayout == null || blockScreen == null)
            return;

        try {
            countLoading--;

            if (countLoading <= 0) {
                blockScreen.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    protected void showDialogList(int arrayId, DialogInterface.OnClickListener itemSelected, int titleID) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titleID);
        builder.setSingleChoiceItems(arrayId, 0, itemSelected);
        builder.setCancelable(false);
        builder.show();
    }

    protected void showDialog(int titleID, int descriptionID, DialogInterface.OnClickListener positiveOption, DialogInterface.OnClickListener negativeOption) {
        showDialog(getString(titleID), getString(descriptionID), positiveOption, negativeOption);
    }

    protected void showDialog(String titleID, String descriptionID, DialogInterface.OnClickListener positiveOption, DialogInterface.OnClickListener negativeOption) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titleID)
                .setMessage(descriptionID).setCancelable(false)
                .setPositiveButton(R.string.ok, positiveOption);
        if (negativeOption != null) {
            builder.setNegativeButton(R.string.cancel, negativeOption);
        }
        builder.show();
    }

    @Override
    public void onRefresh() {
        onRefreshing();
    }

    public abstract void onRefreshing();
}

