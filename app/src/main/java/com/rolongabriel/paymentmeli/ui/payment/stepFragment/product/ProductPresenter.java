package com.rolongabriel.paymentmeli.ui.payment.stepFragment.product;

import com.rolongabriel.paymentmeli.MeLiAplication;
import com.rolongabriel.paymentmeli.data.repository.PaymentRepository;
import com.rolongabriel.paymentmeli.ui.base.BasePresenter;

import javax.inject.Inject;

public class ProductPresenter extends BasePresenter<ProductContractView.View> implements  ProductContractView.Presenter {

    @Inject PaymentRepository repository;

    public ProductPresenter(ProductContractView.View view) {
        super(view);
        ((MeLiAplication)MeLiAplication.context.getApplicationContext()).getPaymentComponent().inject(this);
    }

    @Override
    public void saveAmount(String amount) {
        repository.setAmount(amount);
    }

    public boolean isfinalStep() {
        return repository.getFee() != null && repository.getFee().getRecommendedMessage() != null;
    }

    public String getpaymentTypeSelected() {
        return repository.getPaymentTypeSelected().getName();
    }

    public String getAmountSelected() {
        return repository.getAmount();
    }


    public String getBankSelected() {
        return repository.getBankSelected().getName();
    }

    public String getFeeSelected() {
        return repository.getFee().getRecommendedMessage();
    }

    @Override
    public void clearDataSelected() {
        repository.clearDataSelected();
    }
}
