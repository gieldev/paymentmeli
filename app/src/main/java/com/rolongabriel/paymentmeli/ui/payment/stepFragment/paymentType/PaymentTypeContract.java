package com.rolongabriel.paymentmeli.ui.payment.stepFragment.paymentType;

import com.rolongabriel.paymentmeli.data.model.PaymentType;
import com.rolongabriel.paymentmeli.ui.base.BaseContractView;

import java.util.ArrayList;

public interface PaymentTypeContract {
    interface View extends BaseContractView {

        void onSuccesRequest(ArrayList<PaymentType> paymentTypes);
    }

    interface Presenter{
        void getPaymentType();
    }
}
