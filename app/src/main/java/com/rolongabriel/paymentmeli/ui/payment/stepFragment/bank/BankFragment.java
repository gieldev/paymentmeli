package com.rolongabriel.paymentmeli.ui.payment.stepFragment.bank;

import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.rolongabriel.paymentmeli.MeLiAplication;
import com.rolongabriel.paymentmeli.R;
import com.rolongabriel.paymentmeli.common.OnItemSelected;
import com.rolongabriel.paymentmeli.data.model.BankData;
import com.rolongabriel.paymentmeli.ui.base.BasePaymentFragment;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.fee.FeeFragment;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.paymentType.PaymentTypeAdapter;

import java.util.ArrayList;

public class BankFragment extends BasePaymentFragment implements BankContract.View, OnItemSelected<BankData> {
    public  static final String TAG="BankFragment" ;
    BankPresenter presenter;
    private RecyclerView mRecyclerView;
    private BankAdapter mAdapter;
    private static BankFragment instance;

    public static BankFragment getInstance() {
        if(instance == null)
            instance = new BankFragment();

        return instance;
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.getBank();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }


    @Override
    protected int getLayoutID() {
        return R.layout.generic_recycle_view;
    }

    @Override
    protected void init() {
        setHasOptionsMenu(true);
        presenter = new BankPresenter(this);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.genericRV);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MeLiAplication.context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(new BankAdapter(null, this));


    }

    @Override
    protected String getcurrentTag() {
        return TAG;
    }

    @Override
    protected boolean isBackRequiered() {
        return true;
    }

    @Override
    public void onRefreshing() {
     presenter.getBank();
    }

    @Override
    public void onSuccesRequest(ArrayList<BankData> list) {
        if (mAdapter == null || !(mRecyclerView.getAdapter() instanceof BankAdapter)) {
            mAdapter = new BankAdapter(list, this);
            mRecyclerView.setAdapter(mAdapter);
        } else
            mAdapter.setData(list);
    }

    @Override
    public void lastStepEmpty() {
        showToast(R.string.paymenttype_selected_empty, false);
    }

    @Override
    public void showToastView(String stringID, boolean longDuration) {
        showToast(stringID, false);
    }

    @Override
    public void showToastView(int stringID, boolean longDuration) {
        showToast(stringID, false);
    }

    @Override
    public void showLoadingView() {
        showLoading();
    }

    @Override
    public void dismissLoadingView() {
        dismissLoading();
    }

    @Override
    public void showDialogView(int arrayId, DialogInterface.OnClickListener itemSelected) {
     showDialogList(arrayId, itemSelected, R.string.app_name);
    }

    @Override
    public void onSelected(BankData bankData) {
        presenter.saveBank(bankData);
        nextStep(FeeFragment.TAG);
    }


}
