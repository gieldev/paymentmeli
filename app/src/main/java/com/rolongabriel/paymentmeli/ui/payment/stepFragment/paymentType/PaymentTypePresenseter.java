package com.rolongabriel.paymentmeli.ui.payment.stepFragment.paymentType;

import com.rolongabriel.paymentmeli.MeLiAplication;
import com.rolongabriel.paymentmeli.data.model.PaymentType;
import com.rolongabriel.paymentmeli.data.repository.PaymentRepository;
import com.rolongabriel.paymentmeli.data.repository.datasource.PaymentDataSource;
import com.rolongabriel.paymentmeli.ui.base.BasePresenter;
import com.rolongabriel.paymentmeli.ui.base.MeLiCallBack;


import java.util.ArrayList;

import javax.inject.Inject;

public class PaymentTypePresenseter extends BasePresenter<PaymentTypeContract.View> implements PaymentTypeContract.Presenter{
    @Inject
    PaymentRepository repository;

    public PaymentTypePresenseter(PaymentTypeContract.View view) {
        super(view);
        ((MeLiAplication)MeLiAplication.context.getApplicationContext()).getPaymentComponent().inject(this);
    }

    @Override
    public void getPaymentType() {
        view.showLoadingView();
        repository.getAllPaymentsType(new MeLiCallBack<PaymentDataSource.WrapperPaymentTypeResponse>() {
            @Override
            public void onSuccesResponse(PaymentDataSource.WrapperPaymentTypeResponse response) {
                view.dismissLoadingView();
                view.onSuccesRequest(response.arrayList);
            }

            @Override
            public void onErrorResponse(String error) {
                view.dismissLoadingView();
                view.showToastView(error, false);
                view.onSuccesRequest(new ArrayList<PaymentType>());
            }
        });
    }

    public void savePaymentTypeSelected(PaymentType paymentType) {
        repository.savePaymentType(paymentType);
    }
}
