package com.rolongabriel.paymentmeli.ui.base;

import android.content.Context;
import com.rolongabriel.paymentmeli.ui.payment.MainActivity;

public abstract class BasePaymentFragment extends  BaseFragment {

    private MainActivity activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (MainActivity) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setHeader(getcurrentTag());
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(isBackRequiered());
    }

    protected void nextStep(String tag){
        activity.loadStep(tag);
    }

    protected void onBackPressed(){
        activity.onBackPressed();
    }



    protected void finish() {
        activity.finish();
    }


    protected abstract boolean isBackRequiered();
}
