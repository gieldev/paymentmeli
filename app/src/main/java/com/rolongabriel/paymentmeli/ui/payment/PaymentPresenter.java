package com.rolongabriel.paymentmeli.ui.payment;


import com.rolongabriel.paymentmeli.MeLiAplication;
import com.rolongabriel.paymentmeli.data.model.PaymentType;
import com.rolongabriel.paymentmeli.data.repository.PaymentRepository;
import com.rolongabriel.paymentmeli.data.repository.datasource.PaymentDataSource;
import com.rolongabriel.paymentmeli.ui.base.BasePresenter;
import com.rolongabriel.paymentmeli.ui.base.MeLiCallBack;

import javax.inject.Inject;

public class PaymentPresenter extends BasePresenter<PaymentContract.View> implements PaymentContract.Presenter {
    @Inject PaymentRepository repository;

    public PaymentPresenter(PaymentContract.View view) {
        super(view);
        ((MeLiAplication)MeLiAplication.context.getApplicationContext()).getPaymentComponent().inject(this);
    }

    public boolean isfinalStep() {
        return repository.getFee() != null && repository.getFee().getRecommendedMessage() != null;
    }
}
