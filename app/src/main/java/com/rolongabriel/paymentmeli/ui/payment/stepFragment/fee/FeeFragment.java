package com.rolongabriel.paymentmeli.ui.payment.stepFragment.fee;

import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.rolongabriel.paymentmeli.MeLiAplication;
import com.rolongabriel.paymentmeli.R;
import com.rolongabriel.paymentmeli.common.OnItemSelected;
import com.rolongabriel.paymentmeli.data.model.FeeData;
import com.rolongabriel.paymentmeli.ui.base.BasePaymentFragment;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.paymentType.PaymentTypeAdapter;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.product.ProductFragment;

import java.util.ArrayList;

public class FeeFragment extends BasePaymentFragment implements FeeContracView.View, OnItemSelected<FeeData> {
    public static final String TAG="FeeFragment";

    private static FeeFragment instance;
    private FeePresenter presenter;
    private RecyclerView mRecyclerView;
    private FeeAdapter mAdapter;
    private TextView titleTV;

    public static FeeFragment getInstance(){
        if(instance == null)
            instance = new FeeFragment();

        return instance;
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.getFee();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_fee;
    }

    @Override
    protected void init() {
        setHasOptionsMenu(true);
        presenter = new FeePresenter(this);
        mRecyclerView = rootView.findViewById(R.id.genericRV);
        titleTV = rootView.findViewById(R.id.titleProductTV);
        String product = getString(R.string.product_name)+ " $"+ presenter.getAmount();
        titleTV.setText(product);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MeLiAplication.context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(new FeeAdapter(null, this));

    }

    @Override
    public void onSuccessFeeLoaded(ArrayList<FeeData> list) {
        if (mAdapter == null || !(mRecyclerView.getAdapter() instanceof FeeAdapter)) {
            mAdapter = new FeeAdapter(list, this);
            mRecyclerView.setAdapter(mAdapter);
        } else{
            mAdapter.setData(list);
        }
    }

    @Override
    protected String getcurrentTag() {
        return TAG;
    }

    @Override
    protected boolean isBackRequiered() {
        return true;
    }

    @Override
    public void onRefreshing() {
        presenter.getFee();
    }

    @Override
    public void showToastView(String stringID, boolean longDuration) {
        showToast(stringID, longDuration);
    }

    @Override
    public void showToastView(int stringID, boolean longDuration) {
        showToast(stringID, longDuration);
    }

    @Override
    public void showLoadingView() {
        showLoading();
    }

    @Override
    public void dismissLoadingView() {
        dismissLoading();
    }

    @Override
    public void showDialogView(int arrayId, DialogInterface.OnClickListener itemSelected) {
        showDialogList(arrayId, itemSelected, R.string.app_name);
    }

    @Override
    public void onSelected(FeeData feeData) {
        presenter.saveFeed(feeData);
        nextStep(ProductFragment.TAG);
    }
}
