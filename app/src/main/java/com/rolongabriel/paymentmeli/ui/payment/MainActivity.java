package com.rolongabriel.paymentmeli.ui.payment;

import android.content.DialogInterface;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ImageView;
import android.widget.TextView;

import com.rolongabriel.paymentmeli.R;
import com.rolongabriel.paymentmeli.data.model.PaymentType;
import com.rolongabriel.paymentmeli.ui.base.BaseActivity;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.paymentType.PaymentTypeFragment;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.bank.BankFragment;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.fee.FeeFragment;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.product.ProductFragment;


public class MainActivity extends BaseActivity implements  PaymentContract.View{

    private PaymentPresenter presenter;
    private ImageView stepIV;
    private TextView titleIV;


    @Override
    protected int getLayoutID() {
        return R.layout.activity_main;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_payment, menu);
        return true;
    }

    @Override
    protected void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        presenter = new PaymentPresenter(this);
        stepIV = findViewById(R.id.stepsIV);
        titleIV = findViewById(R.id.titleStepTV);
        loadStep(ProductFragment.TAG);
    }

    @Override
    public void showToastView(String stringID, boolean longDuration) {

    }

    @Override
    public void showToastView(int stringID, boolean longDuration) {

    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void dismissLoadingView() {

    }

    @Override
    public void showDialogView(int arrayId, DialogInterface.OnClickListener itemSelected) {

    }

    @Override
    public void onRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }

    public void setHeader(String tag) {
        int resID = R.drawable.steps;
        switch (tag){
            case ProductFragment.TAG:
                if(presenter.isfinalStep()){
                    titleIV.setText(R.string.final_step);
                    resID = getResources().getIdentifier("steps_4",
                            "drawable", getPackageName());
                }else{
                    titleIV.setText(R.string.start_step);
                    resID = getResources().getIdentifier("steps",
                            "drawable", getPackageName());
                }

                break;

                case PaymentTypeFragment.TAG:
                titleIV.setText(R.string.select_payment_type);
                resID = getResources().getIdentifier("steps_1",
                        "drawable", getPackageName());
                break;   case BankFragment.TAG:
                titleIV.setText(R.string.select_bank);
                resID = getResources().getIdentifier("steps_2",
                        "drawable", getPackageName());
                break;

                case FeeFragment.TAG:
                titleIV.setText(R.string.select_fee);
                resID = getResources().getIdentifier("steps_3",
                        "drawable", getPackageName());
                break;
        }

        stepIV.setImageResource(resID);
    }

    public void loadStep(String step){
        switch (step){
            case ProductFragment.TAG:
                loadFragment(R.id.mainFragmentContiner, ProductFragment.getInstance(), false);
            break;
            case PaymentTypeFragment.TAG:
                loadFragment(R.id.mainFragmentContiner, PaymentTypeFragment.getInstance(), true);
            break;

            case BankFragment.TAG:
                loadFragment(R.id.mainFragmentContiner, BankFragment.getInstance(), true);
            break;

            case FeeFragment.TAG:
                loadFragment(R.id.mainFragmentContiner, FeeFragment.getInstance(), true);
                break;
        }
    }
}
