package com.rolongabriel.paymentmeli.ui.payment.stepFragment.paymentType;


import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.rolongabriel.paymentmeli.MeLiAplication;
import com.rolongabriel.paymentmeli.R;
import com.rolongabriel.paymentmeli.data.model.PaymentType;
import com.rolongabriel.paymentmeli.ui.base.BasePaymentFragment;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.bank.BankFragment;
import com.rolongabriel.paymentmeli.common.OnItemSelected;

import java.util.ArrayList;


public class PaymentTypeFragment extends BasePaymentFragment implements PaymentTypeContract.View, OnItemSelected<PaymentType> {

    public static final String TAG = "PaymentTypeFragment";
    private PaymentTypePresenseter presenter;
    public static PaymentTypeFragment instance;
    private RecyclerView mRecyclerView;
    private PaymentTypeAdapter mAdapter;

    public PaymentTypeFragment() {
        super();
    }

    public static PaymentTypeFragment getInstance() {
        if (instance == null) {
            instance = new PaymentTypeFragment();
        }

        return instance;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }


    @Override
    protected int getLayoutID() {
        return R.layout.generic_recycle_view;
    }

    @Override
    protected void init() {
        setHasOptionsMenu(true);
        presenter = new PaymentTypePresenseter(this);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.genericRV);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MeLiAplication.context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(new PaymentTypeAdapter(null, this));
        presenter.getPaymentType();
    }

    @Override
    protected String getcurrentTag() {
        return TAG;
    }

    @Override
    protected boolean isBackRequiered() {
        return true;
    }

    @Override
    public void onRefreshing() {
        presenter.getPaymentType();
    }

    @Override
    public void showToastView(String stringID, boolean longDuration) {
        showToast(stringID, longDuration);
    }

    @Override
    public void showToastView(int stringID, boolean longDuration) {
        showToast(stringID, longDuration);
    }

    @Override
    public void showLoadingView() {
        showLoading();
    }

    @Override
    public void dismissLoadingView() {
        dismissLoading();
    }

    @Override
    public void showDialogView(int arrayId, DialogInterface.OnClickListener itemSelected) {
        showDialogList(arrayId, itemSelected, R.string.app_name);
    }

    @Override
    public void onSuccesRequest(ArrayList<PaymentType> paymentTypes) {
        if (mAdapter == null || !(mRecyclerView.getAdapter() instanceof PaymentTypeAdapter)) {
            mAdapter = new PaymentTypeAdapter(paymentTypes, this);
            mRecyclerView.setAdapter(mAdapter);
        } else
            mAdapter.setData(paymentTypes);
    }

    @Override
    public void onSelected(PaymentType paymentType) {
        presenter.savePaymentTypeSelected(paymentType);
        nextStep(BankFragment.TAG);
    }
}
