package com.rolongabriel.paymentmeli.ui.payment.stepFragment.fee;

import com.rolongabriel.paymentmeli.MeLiAplication;
import com.rolongabriel.paymentmeli.data.model.FeeData;
import com.rolongabriel.paymentmeli.data.repository.PaymentRepository;
import com.rolongabriel.paymentmeli.data.repository.datasource.PaymentDataSource;
import com.rolongabriel.paymentmeli.ui.base.BasePresenter;
import com.rolongabriel.paymentmeli.ui.base.MeLiCallBack;

import java.util.ArrayList;

import javax.inject.Inject;

public class FeePresenter extends BasePresenter<FeeContracView.View> implements  FeeContracView.Presenter {

@Inject  PaymentRepository repository;

    public FeePresenter(FeeContracView.View view) {
        super(view);
        ((MeLiAplication)MeLiAplication.context.getApplicationContext()).getPaymentComponent().inject(this);
    }

    public void  getFee() {
        view.showLoadingView();
        repository.getAllFee(repository.getAmount(), repository.getPaymentTypeSelected().getId(), repository.getBankSelected().getId(), new MeLiCallBack<PaymentDataSource.WrapperFeeResponse>() {

            @Override
            public void onSuccesResponse(PaymentDataSource.WrapperFeeResponse response) {
                view.dismissLoadingView();
                view.onSuccessFeeLoaded(response.arrayList);
            }

            @Override
            public void onErrorResponse(String error) {
                view.dismissLoadingView();
                view.showToastView(error, false);
                view.onSuccessFeeLoaded(new ArrayList<FeeData>());
            }
        });
    }

    @Override
    public void saveFeed(FeeData feeData) {
        repository.saveFee(feeData);
    }

    public String getAmount() {
        return repository.getAmount();
    }
}
