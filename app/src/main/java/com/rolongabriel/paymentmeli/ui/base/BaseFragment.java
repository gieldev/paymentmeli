package com.rolongabriel.paymentmeli.ui.base;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.rolongabriel.paymentmeli.R;


public abstract class BaseFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    protected View rootView;
    protected Context context;

    //loading
    protected SwipeRefreshLayout swipeRefreshLayout;
    private ImageView blockScreen;
    private int countLoading = 0;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(getLayoutID(), container, false);
            initLoading();

            init();
        }

        return rootView;
    }

    private void initLoading() {
        swipeRefreshLayout = rootView.findViewById(R.id.swipeRefresh);
        if(swipeRefreshLayout!= null){
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark),getResources().getColor(R.color.yellow),getResources().getColor(R.color.blue_dark),getResources().getColor(R.color.blue));
        }

        blockScreen =  rootView.findViewById(R.id.blockScreen);

    }

    protected abstract int getLayoutID();

    protected abstract void init();

    protected abstract String getcurrentTag();


    protected void showToast(int stringID, boolean duration){
        ((BaseActivity)getActivity()).showToast(stringID, duration);
    }

    protected void showToast(String stringID, boolean duration){
        ((BaseActivity)getActivity()).showToast(stringID, duration);
    }

    protected void showLoading() {

        if(swipeRefreshLayout== null || blockScreen== null)
            return;

        blockScreen.setVisibility(View.VISIBLE);

        try {
            if (!swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(true);
            }
            countLoading++;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void dismissLoading() {

        if(swipeRefreshLayout== null || blockScreen== null)
            return;

        try {
            countLoading--;

            if (countLoading <= 0) {
                blockScreen.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    protected void showDialogList(int arrayId, DialogInterface.OnClickListener itemSelected, int titleID){
        ((BaseActivity)context).showDialogList(arrayId, itemSelected, titleID);
    }

    protected void showDialog(int titleID, int descriptionId, DialogInterface.OnClickListener positiveOption, DialogInterface.OnClickListener negativeOption){
        ((BaseActivity)context).showDialog(titleID, descriptionId, positiveOption, negativeOption);
    }

    @Override
    public void onRefresh() {
        onRefreshing();
    }

    public abstract void onRefreshing();
}
