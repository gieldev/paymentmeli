package com.rolongabriel.paymentmeli.ui.payment.stepFragment.fee;

import com.rolongabriel.paymentmeli.data.model.FeeData;
import com.rolongabriel.paymentmeli.ui.base.BaseContractView;

import java.util.ArrayList;

public interface FeeContracView {
    interface  View extends BaseContractView{
        void onSuccessFeeLoaded(ArrayList<FeeData> list);
    }

    interface Presenter{
        public void  getFee();
        void saveFeed(FeeData feeData);
    }
}
