package com.rolongabriel.paymentmeli.ui.base;

public interface MeLiCallBack <t> {
    void onSuccesResponse(t response);
    void onErrorResponse(String error);
}
