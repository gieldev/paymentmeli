package com.rolongabriel.paymentmeli.ui.payment.stepFragment.product;

import com.rolongabriel.paymentmeli.ui.base.BaseContractView;

public interface ProductContractView {
    interface  View extends BaseContractView{
        public void setActionVisible(int actionID, boolean enable);
    }

    interface Presenter{
        void saveAmount(String amount);
        boolean isfinalStep();

        void clearDataSelected();
    }
}
