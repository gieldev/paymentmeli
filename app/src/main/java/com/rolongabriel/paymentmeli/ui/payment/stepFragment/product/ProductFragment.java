package com.rolongabriel.paymentmeli.ui.payment.stepFragment.product;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.rolongabriel.paymentmeli.R;
import com.rolongabriel.paymentmeli.ui.base.BasePaymentFragment;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.paymentType.PaymentTypeFragment;

public class ProductFragment extends BasePaymentFragment implements ProductContractView.View {
    public static final String TAG = "ProductFragment";

    private static ProductFragment instance;
    private EditText suggestET;
    private ProductPresenter presenter;
    private TextView paymentTypeTV, bankTV, feeTV;
    private View detailContainerV;

    private MenuItem suggestAction, finishAction;

    public static ProductFragment getInstance() {
        if (instance == null)
            instance = new ProductFragment();

        return instance;
    }


    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_payment, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        suggestAction = menu.findItem(R.id.action_suggest);
        finishAction = menu.findItem(R.id.action_finish);
        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_suggest:
                setActionVisible(R.id.action_suggest, true);
                presenter.saveAmount(suggestET.getText().toString());
                nextStep(PaymentTypeFragment.TAG);
                break;
            case R.id.action_finish:
                setActionVisible(R.id.action_finish, false);
                presenter.clearDataSelected();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected int getLayoutID() {
        return R.layout.fragment_product;
    }

    @Override
    protected void init() {
        setHasOptionsMenu(true);
        presenter = new ProductPresenter(this);
        suggestET = rootView.findViewById(R.id.suggestET);
        detailContainerV = rootView.findViewById(R.id.detailContainerV);
        paymentTypeTV = rootView.findViewById(R.id.pymenteTypeTV);
        bankTV = rootView.findViewById(R.id.bankTV);
        feeTV = rootView.findViewById(R.id.feeTV);
        suggestET.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH ||
                        i == EditorInfo.IME_ACTION_DONE ||
                        keyEvent != null &&
                                keyEvent.getAction() == KeyEvent.ACTION_DOWN &&
                                keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (keyEvent == null || !keyEvent.isShiftPressed()) {
                        // the user is done typing.
                        showSuggestAction();
                        return true; // consume.
                    }
                }
                return false; // pass on to other listeners.
            }
        });
    }

    private void showSuggestAction() {
        showToastView(R.string.suggest_action, false);
        setActionVisible(R.id.action_suggest, true);
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(suggestET.getWindowToken(), 0);
    }

    @Override
    protected String getcurrentTag() {
        return TAG;
    }

    @Override
    protected boolean isBackRequiered() {
        return false;
    }

    @Override
    public void onRefreshing() {

    }

    private void loadData() {
        if (presenter.isfinalStep()) {
            detailContainerV.setVisibility(View.VISIBLE);
            paymentTypeTV.setText(presenter.getpaymentTypeSelected());
            bankTV.setText(presenter.getBankSelected());
            feeTV.setText(presenter.getFeeSelected());
            suggestET.setText(presenter.getAmountSelected());
            setActionVisible(R.id.action_finish, true);
        } else {
            setActionVisible(R.id.action_finish, false);
            detailContainerV.setVisibility(View.GONE);
            String amount= presenter.getAmountSelected();
            if ( (suggestET.getText()== null || suggestET.getText().toString().trim().isEmpty()) && amount != null && !amount.isEmpty()) {
                suggestET.setText(amount);
                showSuggestAction();
            }
        }
    }

    @Override
    public void showToastView(String stringID, boolean longDuration) {
        showToast(stringID, false);
    }

    @Override
    public void showToastView(int stringID, boolean longDuration) {
        showToast(stringID, false);
    }

    @Override
    public void showLoadingView() {
        showLoading();
    }

    @Override
    public void dismissLoadingView() {
        dismissLoadingView();
    }

    @Override
    public void showDialogView(int arrayId, DialogInterface.OnClickListener itemSelected) {
        showDialogList(arrayId, itemSelected, R.string.app_name);
    }

    @Override
    public void setActionVisible(int actionID, boolean enable) {
        MenuItem item = null;
        switch (actionID) {
            case R.id.action_suggest:
                item = suggestAction;
                break;

            case R.id.action_finish:
                item = finishAction;
                break;
        }

        if (item != null)
            item.setVisible(enable);
    }
}
