package com.rolongabriel.paymentmeli.ui.payment.stepFragment.bank;

import com.rolongabriel.paymentmeli.data.model.BankData;
import com.rolongabriel.paymentmeli.ui.base.BaseContractView;

import java.util.ArrayList;

public interface BankContract {
    interface View extends BaseContractView{
        void onSuccesRequest(ArrayList<BankData> list);
        void lastStepEmpty();
    }

    interface  Presenter{
        void getBank();
        void saveBank(BankData bankData);
    }
}
