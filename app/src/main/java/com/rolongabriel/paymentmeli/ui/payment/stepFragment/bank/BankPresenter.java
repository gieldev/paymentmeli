package com.rolongabriel.paymentmeli.ui.payment.stepFragment.bank;

import com.rolongabriel.paymentmeli.MeLiAplication;
import com.rolongabriel.paymentmeli.data.model.BankData;
import com.rolongabriel.paymentmeli.data.model.PaymentType;
import com.rolongabriel.paymentmeli.data.repository.PaymentRepository;
import com.rolongabriel.paymentmeli.data.repository.datasource.PaymentDataSource;
import com.rolongabriel.paymentmeli.ui.base.BasePresenter;
import com.rolongabriel.paymentmeli.ui.base.MeLiCallBack;

import java.util.ArrayList;

import javax.inject.Inject;

public class BankPresenter extends BasePresenter <BankContract.View> implements BankContract.Presenter{

    @Inject public PaymentRepository repository;

    public BankPresenter(BankContract.View view) {
        super(view);
        ((MeLiAplication)MeLiAplication.context.getApplicationContext()).getPaymentComponent().inject(this);
    }

    @Override
    public void getBank() {
        PaymentType paymentType = repository.getPaymentTypeSelected();
        String paymentID=null;
        if(paymentType== null || paymentType.getId()== null ){
            view.lastStepEmpty();
        }else {
            paymentID = paymentType.getId();

        }
        view.showLoadingView();
        repository.getAllBank(paymentID, new MeLiCallBack<PaymentDataSource.WrapperBankResponse>() {
            @Override
            public void onSuccesResponse(PaymentDataSource.WrapperBankResponse response) {
                view.dismissLoadingView();
                view.onSuccesRequest(response.arrayList);
            }

            @Override
            public void onErrorResponse(String error) {
                view.dismissLoadingView();
                view.showToastView(error, false);
                view.onSuccesRequest(new ArrayList<BankData>());
            }
        });
    }

    @Override
    public void saveBank(BankData bankData) {
        repository.saveBank(bankData);
    }
}
