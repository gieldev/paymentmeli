package com.rolongabriel.paymentmeli.common;

import android.os.Handler;
import android.os.Looper;

public class ThreadsUtil {


    public static void runtInThread(Runnable runnable){
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public static void runtInBackgroundMainThread(Runnable runnable){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(runnable);
    }


}
