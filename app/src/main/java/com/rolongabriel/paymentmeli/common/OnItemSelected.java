package com.rolongabriel.paymentmeli.common;

public interface OnItemSelected <Data> {
    public void onSelected(Data data);
}
