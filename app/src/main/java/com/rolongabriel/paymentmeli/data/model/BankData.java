package com.rolongabriel.paymentmeli.data.model;

import com.google.gson.annotations.SerializedName;

public class BankData {
    private String id;
    private String name;
    @SerializedName("secure_thumbnail")
    private String secureThumbnail;


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

}
