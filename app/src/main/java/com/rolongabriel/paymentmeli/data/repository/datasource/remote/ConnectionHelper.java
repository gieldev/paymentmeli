package com.rolongabriel.paymentmeli.data.repository.datasource.remote;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class ConnectionHelper {
    private static ConnectionHelper mConnectionHelperInstance;
    private RequestQueue mRequestQueue;
    private static Context mContext;

    private ConnectionHelper(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized ConnectionHelper getInstance(Context context) {
        if (mConnectionHelperInstance == null) {
            mConnectionHelperInstance = new ConnectionHelper(context);
        }
        return mConnectionHelperInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req,String tag) {
        req.setTag(tag);
        getRequestQueue().add(req);
    }

    public  void cancelRequest(String tag){
        if(mRequestQueue!= null)
            mRequestQueue.cancelAll(tag);
    }
}