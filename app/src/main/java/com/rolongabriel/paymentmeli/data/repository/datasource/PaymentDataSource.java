package com.rolongabriel.paymentmeli.data.repository.datasource;


import com.google.gson.annotations.SerializedName;
import com.rolongabriel.paymentmeli.data.model.BankData;
import com.rolongabriel.paymentmeli.data.model.FeeData;
import com.rolongabriel.paymentmeli.data.model.PaymentType;
import com.rolongabriel.paymentmeli.ui.base.MeLiCallBack;

import java.util.ArrayList;

public interface PaymentDataSource {
    void getAllPaymentsType(MeLiCallBack<WrapperPaymentTypeResponse> callBack);
    void getAllBank(String paymentTypeID, MeLiCallBack<WrapperBankResponse> callBack);
    void getAllFee(String amount , String paymentTypeID, String bankID, MeLiCallBack<WrapperFeeResponse> callBack);


    class WrapperPaymentTypeResponse {
        public ArrayList<PaymentType> arrayList;
    }

    class WrapperBankResponse {
        public ArrayList<BankData> arrayList;
    }

    class WrapperFeeResponse {
        @SerializedName("payer_costs")
        public ArrayList<FeeData> arrayList;
    }


}
