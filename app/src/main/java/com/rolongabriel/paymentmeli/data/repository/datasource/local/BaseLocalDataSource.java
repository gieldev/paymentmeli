package com.rolongabriel.paymentmeli.data.repository.datasource.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.rolongabriel.paymentmeli.MeLiAplication;

public class BaseLocalDataSource {
    private SharedPreferences preferences;
    private final String PREFECENCE_NAME="meli_preferences_123aa";


    public BaseLocalDataSource(){
        preferences = MeLiAplication.context.getSharedPreferences(PREFECENCE_NAME, Context.MODE_PRIVATE);
    }


    protected void saveStringInPreference(String key, String value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    protected String getStringFromPreference(String key, String defaultValue){
        return preferences.getString(key,defaultValue);
    }

    protected void removePreference(String key) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

}
