package com.rolongabriel.paymentmeli.data.model;

import com.google.gson.annotations.SerializedName;

public class PaymentType {
    private String id;
    private String name;
    @SerializedName("secure_thumbnail")
    private String secureThumbnail;


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public String getImage_url() {
        return secureThumbnail;
    }


}
