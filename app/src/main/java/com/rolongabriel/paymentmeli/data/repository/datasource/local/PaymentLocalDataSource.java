package com.rolongabriel.paymentmeli.data.repository.datasource.local;


import com.google.gson.Gson;
import com.rolongabriel.paymentmeli.data.model.BankData;
import com.rolongabriel.paymentmeli.data.model.FeeData;
import com.rolongabriel.paymentmeli.data.model.PaymentType;

public class PaymentLocalDataSource extends BaseLocalDataSource {


    private final String KEY_PAYMENT_TYPE ="KEY_PAYMENT_TYPE";
    private final String KEY_BANK_DATA ="KEY_BANK_DATA";
    private final String KEY_FEE_DATA ="KEY_FEE_DATA";
    private final String KEY_AMOUNT ="KEY_AMOUNT";


    public PaymentLocalDataSource() {
        super();
    }


    public PaymentType getPaymentType() {
        Gson gson = new Gson();
        return gson.fromJson(getStringFromPreference(KEY_PAYMENT_TYPE,null), PaymentType.class);
    }


    public void setPaymentType(PaymentType paymentType) {
        Gson gson = new Gson();
        saveStringInPreference(KEY_PAYMENT_TYPE, gson.toJson(paymentType));
    }

    public void setBankData(BankData bankData) {
        Gson gson = new Gson();
        saveStringInPreference(KEY_BANK_DATA, gson.toJson(bankData));
    }

    public BankData getBankSelected() {
        Gson gson = new Gson();
        return gson.fromJson(getStringFromPreference(KEY_BANK_DATA,null), BankData.class);
    }

    public void setFeeData(FeeData feeData) {
        Gson gson = new Gson();
        saveStringInPreference(KEY_FEE_DATA, gson.toJson(feeData));
    }

    public FeeData getFeeDataSelected() {
        Gson gson = new Gson();
        return gson.fromJson(getStringFromPreference(KEY_FEE_DATA,null), FeeData.class);
    }

    public String getAmount() {
        return getStringFromPreference(KEY_AMOUNT,null);
    }

    public void setAmount(String amount) {
        saveStringInPreference(KEY_AMOUNT, amount);
    }

    public void removePaymetTypeSelected() {
        removePreference(KEY_PAYMENT_TYPE);
    }


    public void removeAmountSelected() {
        removePreference(KEY_AMOUNT);
    }

    public void removeBankSelected() {
        removePreference(KEY_BANK_DATA);
    }

    public void removeFeeSelected() {
        removePreference(KEY_FEE_DATA);
    }
}
