package com.rolongabriel.paymentmeli.data.repository;


import com.rolongabriel.paymentmeli.data.model.BankData;
import com.rolongabriel.paymentmeli.data.model.FeeData;
import com.rolongabriel.paymentmeli.data.model.PaymentType;
import com.rolongabriel.paymentmeli.data.repository.datasource.PaymentDataSource;
import com.rolongabriel.paymentmeli.data.repository.datasource.remote.PaymentRemoteDataSource;
import com.rolongabriel.paymentmeli.data.repository.datasource.local.PaymentLocalDataSource;
import com.rolongabriel.paymentmeli.ui.base.MeLiCallBack;

public class PaymentRepository implements PaymentDataSource {

    //have to use data base but i don have time
    PaymentLocalDataSource local;
    PaymentRemoteDataSource remote;

    public PaymentRepository(){
        local = new PaymentLocalDataSource();
        remote = new PaymentRemoteDataSource();
    }


    @Override
    public void getAllPaymentsType(MeLiCallBack<WrapperPaymentTypeResponse> callBack) {
        remote.getAllPaymentsType(callBack);
    }

    public void savePaymentType(PaymentType paymentType){
        local.setPaymentType(paymentType);
    }
    public PaymentType getPaymentTypeSelected(){
        return local.getPaymentType();
    }
    public void saveBank(BankData bankData){
        local.setBankData(bankData);
    }

    public BankData getBankSelected(){
        return  local.getBankSelected();
    }

    @Override
    public void getAllBank(String paymentTypeID, MeLiCallBack<WrapperBankResponse> callBack) {
        remote.getAllBank(paymentTypeID, callBack);
    }

    @Override
    public void getAllFee(String amount, String paymentTypeID, String bankID, MeLiCallBack<WrapperFeeResponse> callBack) {
        remote.getAllFee(amount, paymentTypeID, bankID, callBack);
    }


    public void saveFee(FeeData feeData) {
        local.setFeeData(feeData);
    }

    public FeeData getFee() {
        return  local.getFeeDataSelected();
    }



    public String getAmount() {
        return local.getAmount();
    }

    public void setAmount(String amount){
        local.setAmount(amount);
    }

    public void clearDataSelected() {
        local.removePaymetTypeSelected();
        local.removeAmountSelected();
        local.removeBankSelected();
        local.removeFeeSelected();
    }
}
