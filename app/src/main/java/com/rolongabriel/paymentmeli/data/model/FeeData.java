package com.rolongabriel.paymentmeli.data.model;

import com.google.gson.annotations.SerializedName;

public class FeeData {
    @SerializedName("recommended_message")
    private String recommendedMessage;
    @SerializedName("total_amount")
    private float total;


    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public float getTotal() {
        return total;
    }


}
