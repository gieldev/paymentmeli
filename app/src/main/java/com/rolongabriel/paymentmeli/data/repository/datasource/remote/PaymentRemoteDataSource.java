package com.rolongabriel.paymentmeli.data.repository.datasource.remote;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.rolongabriel.paymentmeli.BuildConfig;
import com.rolongabriel.paymentmeli.common.ThreadsUtil;
import com.rolongabriel.paymentmeli.data.repository.datasource.PaymentDataSource;
import com.rolongabriel.paymentmeli.ui.base.MeLiCallBack;

import org.json.JSONArray;
import org.json.JSONException;


public class PaymentRemoteDataSource extends BaseRemoteDataSource implements PaymentDataSource {

    @Override
    public void getAllPaymentsType(final MeLiCallBack<WrapperPaymentTypeResponse> callBack) {
        String URL = API_URL_BASE + API_PAYMENT_METHOD;

        String apiKey = BuildConfig.MELI_API_KEY;
        String uri = URL+"?"+KEY_API_KEY+"="+apiKey;

        connect(new StringRequest(Request.Method.GET,
                uri, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(final String response) {
                ThreadsUtil.runtInThread(new Runnable() {
                    @Override
                    public void run() {


                        ThreadsUtil.runtInBackgroundMainThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    Gson gson  = new Gson();
                                    String responseTaged = "{\"arrayList\":"+response+"}";
                                     WrapperPaymentTypeResponse wrapperResponse=  gson.fromJson(responseTaged, WrapperPaymentTypeResponse.class);
                                     if(wrapperResponse!= null){
                                         callBack.onSuccesResponse(wrapperResponse);
                                     }else{
                                         callBack.onErrorResponse(getErrorResponseBadFormat());
                                     }

                                }catch (JsonSyntaxException e){
                                    callBack.onErrorResponse(getErrorResponseBadFormat());
                                }

                            }
                        });

                    }
                });
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try{
                    callBack.onErrorResponse(getMessageError(error));
                }catch (NullPointerException e){
                    callBack.onErrorResponse(getErrorResponseDefault());
                }

            }
        }) {

        });
    }

    @Override
    public void getAllBank(String paymentTypeId, final MeLiCallBack<WrapperBankResponse> callBack) {
        String URL = API_URL_BASE + API_PAYMENT_METHOD+API_BANK;

        String apiKey = BuildConfig.MELI_API_KEY;
        String uri = URL+"?"+KEY_API_KEY+"="+apiKey+"&"+KEY_PAYMENT_METHOD+"="+ paymentTypeId;

        connect(new StringRequest(Request.Method.GET,
                uri, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(final String response) {
                ThreadsUtil.runtInThread(new Runnable() {
                    @Override
                    public void run() {


                        ThreadsUtil.runtInBackgroundMainThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Gson gson  = new Gson();
                                    String responseTaged = "{\"arrayList\":"+response+"}";
                                    callBack.onSuccesResponse(gson.fromJson(responseTaged, WrapperBankResponse.class));
                                }catch (JsonSyntaxException e){
                                    callBack.onErrorResponse(getErrorResponseBadFormat());
                                }

                            }
                        });

                    }
                });
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try{
                    callBack.onErrorResponse(getMessageError(error));
                }catch (NullPointerException e){
                    callBack.onErrorResponse(getErrorResponseDefault());
                }

            }
        }) {

        });
    }

    @Override
    public void getAllFee(String amount, String paymentTypeID, String bankID, final MeLiCallBack<WrapperFeeResponse> callBack) {
        String URL = API_URL_BASE+ API_PAYMENT_METHOD + API_FEE;

        String apiKey = BuildConfig.MELI_API_KEY;

        String uri = URL+"?"+KEY_API_KEY+"="+apiKey+"&"+KEY_AMOUNT+"="+amount+"&"+KEY_PAYMENT_METHOD+"="+ paymentTypeID+"&"+KEY_ISSUER+"=" + bankID;

        connect(new StringRequest(Request.Method.GET,
                uri, new com.android.volley.Response.Listener<String>() {

            @Override
            public void onResponse(final String response) {
                ThreadsUtil.runtInThread(new Runnable() {
                    @Override
                    public void run() {


                        ThreadsUtil.runtInBackgroundMainThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    String resposeJson = null;
                                        resposeJson = new JSONArray(response).getJSONObject(0).toString();
                                    Gson gson  = new Gson();
                                    callBack.onSuccesResponse(gson.fromJson(resposeJson, WrapperFeeResponse.class));
                                }catch (JsonSyntaxException e){
                                    callBack.onErrorResponse(getErrorResponseBadFormat());
                                }catch (JSONException e) {
                                    e.printStackTrace();
                                    callBack.onErrorResponse(getErrorResponseBadFormat());
                                }

                            }
                        });

                    }
                });
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                try{
                    callBack.onErrorResponse(getMessageError(error));
                }catch (NullPointerException e){
                    callBack.onErrorResponse(getErrorResponseDefault());
                }

            }
        }) {

        });
    }
}
