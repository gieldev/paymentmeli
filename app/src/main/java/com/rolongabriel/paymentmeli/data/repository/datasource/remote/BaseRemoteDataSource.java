package com.rolongabriel.paymentmeli.data.repository.datasource.remote;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.rolongabriel.paymentmeli.MeLiAplication;
import com.rolongabriel.paymentmeli.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;


public class BaseRemoteDataSource {
    protected final String API_URL_BASE = "https://api.mercadopago.com";

    protected final String API_PAYMENT_METHOD = "/v1/payment_methods";
    protected final String API_BANK = "/card_issuers";
    protected final String API_FEE = "/installments";


    protected final String KEY_API_KEY = "public_key";
    protected final String KEY_PAYMENT_METHOD = "payment_method_id";
    protected final String KEY_AMOUNT = "amount";
    protected final String KEY_ISSUER = "issuer.id";



    private final String KEY_ERROR_MESSAGE= "message";

    protected void connect(StringRequest strReq) {
        ConnectionHelper.getInstance(MeLiAplication.context).addToRequestQueue(strReq, strReq.getUrl());
    }

    protected void cancelRequest(String tag) {
        ConnectionHelper.getInstance(MeLiAplication.context).cancelRequest(tag);
    }

    protected String getMessageError(VolleyError error) {
        try {
            String response = new String(error.networkResponse.data, "UTF-8");
            JSONObject jsonObject = new JSONObject(response);
            String message = jsonObject.getString(KEY_ERROR_MESSAGE);
            if (message != null && !message.isEmpty())
                return message;
            else
                return getErrorResponseBadFormat();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return getErrorResponseDefault();
        } catch (JSONException e) {
            e.printStackTrace();
            return getErrorResponseBadFormat();
        }
    }

    public String getErrorResponseDefault() {
        return MeLiAplication.context.getString(R.string.error_response);
    }

    public String getErrorResponseBadFormat() {
        return MeLiAplication.context.getString(R.string.error_response_bad_format);
    }

}
