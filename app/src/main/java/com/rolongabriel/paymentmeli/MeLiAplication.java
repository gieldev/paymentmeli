package com.rolongabriel.paymentmeli;

import android.app.Application;
import android.content.Context;

import com.rolongabriel.paymentmeli.di.DaggerPaymentComponent;
import com.rolongabriel.paymentmeli.di.PaymentComponent;
import com.rolongabriel.paymentmeli.di.PaymentModule;

public class MeLiAplication extends Application {

    public static Context context;

    private PaymentComponent paymentComponent;




    @Override
    public void onCreate() {
        super.onCreate();
       MeLiAplication.context = this;
        paymentComponent = DaggerPaymentComponent.builder().paymentModule(new PaymentModule()).build();
    }

    public PaymentComponent getPaymentComponent(){
        return paymentComponent;
    }

}
