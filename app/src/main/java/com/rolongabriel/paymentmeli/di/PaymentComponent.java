package com.rolongabriel.paymentmeli.di;

import com.rolongabriel.paymentmeli.ui.payment.PaymentPresenter;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.bank.BankPresenter;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.fee.FeePresenter;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.paymentType.PaymentTypePresenseter;
import com.rolongabriel.paymentmeli.ui.payment.stepFragment.product.ProductPresenter;

import dagger.Component;

@Component(modules={PaymentModule.class})
public interface PaymentComponent {
 void inject(PaymentTypePresenseter presenter);
 void inject(FeePresenter presenter);
 void inject(BankPresenter presenter);
 void inject(PaymentPresenter presenter);
 void inject(ProductPresenter presenter);
}
