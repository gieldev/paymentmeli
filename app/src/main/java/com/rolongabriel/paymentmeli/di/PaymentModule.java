package com.rolongabriel.paymentmeli.di;

import com.rolongabriel.paymentmeli.data.repository.PaymentRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class PaymentModule {

    @Provides
    public PaymentRepository providePaymentRepository(){
        return new PaymentRepository();
    }

}
